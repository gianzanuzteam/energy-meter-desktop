#-------------------------------------------------
#
# Project created by QtCreator 2016-06-09T13:53:02
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = EnergyMeter
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    measures.cpp

HEADERS += mainwindow.h \
    qcustomplot.h \
    measures.h

FORMS   += mainwindow.ui

RC_ICONS = favicon.ico
