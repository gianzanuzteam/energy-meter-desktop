#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
#include "measures.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QCloseEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    /* Setup Inicial */
    void setupMainWindow();

    /* Gráfico */
    void graphPlotClear(void);
    void graphPlotAdd(int graph, quint8 sensorID, QVector<double> x, QVector<double> y);
    void graphPlotShow(QString yLabel);
    void on_GraphDoubleClick();
    void refresh_plot(int index);

    /* Dados e Dias */
    void on_SensorDataButton_clicked();
    void on_SensorDateClearButton_clicked();
    void on_DateTableWidget_itemSelectionChanged();
    void DateTableWidget_refresh();
    void refresh_date_list(QList<QDate> &dateList);

    /* Carregar/Salvar Arquivo */
    void on_fileLoadButton_clicked();
    void on_FileSaveButton_clicked();

    /* Servidor */
    void on_pushButton_connect_clicked();
    void on_pushButton_edit_clicked();
    void on_lineEdit_key_textChanged(const QString &arg1);
    void on_lineEdit_channel_textChanged(const QString &arg1);
    void networkFinished(QNetworkReply* reply);

    /* Medida elétrica */
    void on_comboBox_medida_currentIndexChanged(int index);
    void on_radioButton_127v_clicked();
    void on_radioButton_220v_clicked();
    void on_comboBox_bandeira_currentIndexChanged(int index);
    void on_doubleSpinBox_custo_valueChanged(double arg1);

    /* Configurações */
    void save_configuration(void);
    void load_configuration(void);
    void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow *ui;             /* Widgets */
    Measures measures{};      /* Gerenciamento dos dados e medidas */
    QVector<QColor> colorList{};    /* Vetor com a lista de cores dos gráficos */
    QCPPlotTitle* graphTitle;       /* Título do gráfico */
    QString plotTitle{};            /* String para o título do gráfico */
    QList<QDate> selectedDate{};    /* Lista com os dias selecionados */
    QNetworkAccessManager *networkManager;  /* Gerenciador p/ conexão com servidor */

    /* Medida elétrica */
    int voltage;
    double bandeira;
    double custo;
};

#endif // MAINWINDOW_H
