#include "measures.h"


/***
 * PUBLIC FUNCTIONS *
 *                  ***/

/* Limpa todos os dados armazenados */
void Measures::clear_sensorData(void)
{
    sensorData.clear();
    sensorDateHash.clear();
    sensorDataEntry.clear();
}

/* Adiciona os dados recebidos de cada mensagem */
void Measures::set_sensorData(QJsonObject &json)
{
    /* Verifica o tamanho */
    if(!json.size())
        return;

    QDateTime timestamp = QDateTime::fromString(json.value("created_at").toString(),Qt::ISODate);
    timestamp.setTimeSpec(Qt::UTC);
    int entryID = json.value("entry_id").toInt();
    double field1 = json.value("field1").toString().toDouble()/1000.0;
    double field2 = json.value("field2").toString().toDouble()/1000.0;

    /* Verifica pelo numero sequencial da mensagem */
    /* Caso já possua, descarta mensagem */
    if(!sensorDataEntry.contains(entryID))
        sensorDataEntry.append(entryID);
    else
        return;

    /* Obtém index da struct de dias, baseado no dia recebido pela mensagem */
    /* Caso o dia recebido não esteja registrado, o adiciona à lista e à hash */
    int dateIndex = sensorDateHash.value(timestamp.date(), -1);
    if(dateIndex == -1)
    {
        sensor_data_t sensorDataEmpty{};
        sensorData.append(sensorDataEmpty);
        dateIndex = sensorData.size() - 1;
        sensorDateHash[timestamp.date()] = dateIndex;
    }

    sensorData[dateIndex].active = true;
    sensorData[dateIndex].entryID.append(entryID);
    sensorData[dateIndex].timestamp.append(timestamp.toTime_t());
    sensorData[dateIndex].data1.append(field1);
    sensorData[dateIndex].data2.append(field2);
}

/* Função auxiliar para ordenar por data */
bool compareByDate(QPair<QDate,quint32> &a, QPair<QDate,quint32> &b)
{
    return a.first < b.first;
}

/* Retorna uma lista com todos os dias que possuem dados armazenados */
QList<QPair<QDate,quint32>> Measures::get_active_dateList()
{
    QList<QPair<QDate,quint32>> dateSizeList{};
    QPair<QDate,quint32> dateSize{};

    for(int i=0; i<sensorData.size(); i++)
    {
        dateSize.first = sensorDateHash.key(i);
        dateSize.second =  sensorData[i].entryID.size();
        dateSizeList.append(dateSize);
    }
    std::sort(dateSizeList.begin(),dateSizeList.end(),compareByDate);   /* Ordena por data */

    return dateSizeList;
}

/* Obtém todos os dados de uma lista de dias */
Measures::sensor_data_t Measures::get_sensorData(QList<QDate> dateList)
{
    sensor_data_t sensorDataTemp{};

    /* Interação para cada dia da lista solicitada */
    for(int j=0; j<dateList.size(); j++)
    {
        /* Obtém o index através do dia */
        int dateIndex = sensorDateHash.value(dateList[j], -1);
        if(dateIndex == -1)
            continue;
        sensorDataTemp.data1.append(sensorData[dateIndex].data1);
        sensorDataTemp.data2.append(sensorData[dateIndex].data2);
        sensorDataTemp.timestamp.append(sensorData[dateIndex].timestamp);
        sensorDataTemp.entryID.append(sensorData[dateIndex].entryID);
        sensorDataTemp.active = true;
    }

    return sensorDataTemp;
}
