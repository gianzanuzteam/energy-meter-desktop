#ifndef MEASURES_H
#define MEASURES_H

#include <QDateTime>
#include <QVector>
#include <QHash>
#include <QJsonDocument>
#include <QJsonObject>

#define DATA_CHANNEL_SIZE 2

class Measures
{
public:

    struct sensor_data_t
    {
        bool active;
        QVector<int> entryID;
        QVector<double> data1;
        QVector<double> data2;
        QVector<double> timestamp;
    };

private:
    /* Listas de dados */
    QList<sensor_data_t> sensorData{};              /* Lista principal com os dados separados para cada dia */
    QList<int> sensorDataEntry{};                   /* Lista que guarda o numero sequencial de cada mensagem */
    QHash<QDate, int> sensorDateHash{};             /* Hash que relaciona o dia com o index da lista 'sensorData' */

public:
    /* Limpa todos os dados armazenados */
    void clear_sensorData(void);

    /* Adiciona os dados recebidos de cada mensagem */
    void set_sensorData(QJsonObject &json);

    /* Retorna uma lista com todos os dias ativos */
    QList<QPair<QDate,quint32>> get_active_dateList();

    /* Obtém todos os dados de uma lista de dias */
    sensor_data_t get_sensorData(QList<QDate> dateList);
};

#endif // MEASURES_H
