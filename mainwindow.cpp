#include "mainwindow.h"
#include "ui_mainwindow.h"

enum combobox_medida_t {
    COMBOBOX_CORRENTE_ELETRICA   = 0,
    COMBOBOX_CONSUMO,
    COMBOBOX_CONSUMO_ACUMULADO,
    COMBOBOX_CUSTO,
    COMBOBOX_CUSTO_ACUMULADO,
};

const QStringList combobox_medida_labels = {
    "Corrente Elétrica [A]",
    "Consumo de Energia [kWh]",
    "Consumo de Energia Acumulado [kWh]",
    "Custo de Energia [R$]",
    "Custo de Energia Acumulado [R$]",
};

const QStringList combobox_medida_unidades = {
    "Ámperes [A]",
    "kiloWatts Hora [kWh]",
    "kiloWatts Hora [kWh]",
    "Reais [R$]",
    "Reais [R$]"
};

enum combobox_bandeira_t {
    COMBOBOX_VERDE,
    COMBOBOX_AMARELA,
    COMBOBOX_VERMELHA_1,
    COMBOBOX_VERMELHA_2,
};

const QStringList combobox_bandeira_labels = {
    "Verde (Sem adicional)",
    "Amarela",
    "Vermelha Patamar I",
    "Vermelha Patamar II",
};

const QList<double> combobox_bandeira_valor = {
    1.0,
    1.02,
    1.03,
    1.05,
};

enum config_type_t {
    CONFIG_CHANNEL,
    CONFIG_KEY,
    CONFIG_VOLTAGE,
    CONFIG_COST,
    CONFIG_FLAG,
};

const QStringList config_type_labels = {
    "channel",
    "key",
    "voltage",
    "cost",
    "flag",
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupMainWindow();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/************************************************************************************
 *                                  SETUP INICIAL                                   *
 * *********************************************************************************/
/* Estado inicial dos Widgets */
void MainWindow::setupMainWindow(void)
{
    networkManager = new QNetworkAccessManager(this);
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(networkFinished(QNetworkReply*)));

    /* Measures */
    ui->SensorDataButton->setEnabled(false);

    /* Date */
    ui->DateCalendar->setEnabled(false);
    ui->DateCalendar->setGridVisible(true);

    /* Graph */
    graphTitle = new QCPPlotTitle(ui->customPlot, "");
    ui->customPlot->plotLayout()->insertRow(0);
    ui->customPlot->plotLayout()->addElement(0, 0, graphTitle);
    connect(ui->customPlot,SIGNAL(mouseDoubleClick(QMouseEvent*)), this, SLOT(on_GraphDoubleClick()));

    /* Combobox medida */
    ui->comboBox_medida->addItems(combobox_medida_labels);
    ui->comboBox_medida->setCurrentIndex(COMBOBOX_CORRENTE_ELETRICA);

    /* Combobox bandeira */
    ui->comboBox_bandeira->addItems(combobox_bandeira_labels);
    ui->comboBox_bandeira->setCurrentIndex(COMBOBOX_VERDE);
    bandeira = combobox_bandeira_valor[COMBOBOX_VERDE];

    /* Measures */
    ui->radioButton_127v->setChecked(true);
    voltage = 127;

    /* Data */
    ui->SensorDateClearButton->setEnabled(false);
    measures.clear_sensorData();

    /* progressBar */
    ui->progressBar->setRange(0,100);
    ui->progressBar->reset();
    ui->progressBar->setEnabled(false);

    /* DateTable */
    ui->DateTableWidget->verticalHeader()->hide();
    ui->DateTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->DateTableWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->DateTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->DateTableWidget->insertColumn(0);
    ui->DateTableWidget->insertColumn(1);
    ui->DateTableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Data",0));
    ui->DateTableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Tamanho",0));
    ui->DateTableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    /* Botão de salvar dados */
    ui->FileSaveButton->setEnabled(false);

    /* Carregar configurações do arquivo */
    load_configuration();
}

/************************************************************************************
 *                                  GRÁFICOS                                        *
 * *********************************************************************************/
/* Limpa os gráficos anteriores */
void MainWindow::graphPlotClear(void)
{
    graphTitle->setText("");
    ui->customPlot->legend->setVisible(false);
    ui->customPlot->legend->clear();
    ui->customPlot->legend->clearItems();
    ui->customPlot->clearGraphs();
    ui->customPlot->clearItems();
    ui->customPlot->clearFocus();
    ui->customPlot->clearMask();
    ui->customPlot->clearPlottables();
    ui->customPlot->replot();
    ui->customPlot->setEnabled(false);
}

/* Adiciona dados aos gráficos */
void MainWindow::graphPlotAdd(int graph, quint8 sensorID, QVector<double> x, QVector<double> y)
{
    QPen pen;
    pen.setColor(colorList[graph]);

    ui->customPlot->addGraph();
    ui->customPlot->graph(graph)->setLineStyle(QCPGraph::lsLine);
    ui->customPlot->graph(graph)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 10));
    ui->customPlot->graph(graph)->setPen(pen);
    ui->customPlot->graph(graph)->setData(x, y);
    ui->customPlot->graph(graph)->rescaleAxes((bool) graph);
    ui->customPlot->graph(graph)->setName("Entrada " + QString::number(sensorID));
}

/* Exibe o gráfico e aplica interfaces */
void MainWindow::graphPlotShow(QString yLabel)
{
    /* Incrementa range em +-5% */
    /* Verifica limites negativos */
    double yUpper = ui->customPlot->yAxis->range().upper;
    double yLower = ui->customPlot->yAxis->range().lower;
    if(yUpper > 0 && yLower > 0)
        ui->customPlot->yAxis->setRange(yLower*0.95,yUpper*1.05);
    else if(yUpper > 0 && yLower < 0)
        ui->customPlot->yAxis->setRange(yLower*1.05,yUpper*1.05);
    else if(yUpper < 0 && yLower > 0)
        ui->customPlot->yAxis->setRange(yLower*0.95,yUpper*0.95);
    else if(yUpper < 0 && yLower < 0)
        ui->customPlot->yAxis->setRange(yLower*1.05,yUpper*0.95);

    // configure bottom axis to show date and time instead of number:
    ui->customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    ui->customPlot->xAxis->setDateTimeFormat("hh:mm\ndd/MM/yy");
    ui->customPlot->xAxis->setLabel("Hora");

    //Configure y axis
    ui->customPlot->yAxis->setLabel(yLabel);

    //title
    graphTitle->setText("Medida: " + plotTitle);

    // show legend:
    ui->customPlot->legend->setVisible(true);

    // add interactions
    ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
    ui->customPlot->replot();
    ui->customPlot->setEnabled(true);
}

/* Evento de double-click no gráfico */
/* Restaura zoom */
void MainWindow::on_GraphDoubleClick(void)
{
    ui->customPlot->rescaleAxes();

    /* Incrementa range em +-5% */
    /* Verifica limites negativos */
    double yUpper = ui->customPlot->yAxis->range().upper;
    double yLower = ui->customPlot->yAxis->range().lower;
    if(yUpper > 0 && yLower > 0)
        ui->customPlot->yAxis->setRange(yLower*0.95,yUpper*1.05);
    else if(yUpper > 0 && yLower < 0)
        ui->customPlot->yAxis->setRange(yLower*1.05,yUpper*1.05);
    else if(yUpper < 0 && yLower > 0)
        ui->customPlot->yAxis->setRange(yLower*0.95,yUpper*0.95);
    else if(yUpper < 0 && yLower < 0)
        ui->customPlot->yAxis->setRange(yLower*1.05,yUpper*0.95);

    ui->customPlot->replot();
}

/* Atualiza o gráfico */
void MainWindow::refresh_plot(int index)
{
    graphPlotClear();

    /* Verifica se combobox não está vazia */
    if(index < 0)
        return;

    /* Verifica se a lista de dias selecionados não está vazia */
    if(selectedDate.isEmpty())
        return;

    Measures::sensor_data_t sensorData{};
    sensorData = measures.get_sensorData(selectedDate);

    colorList.clear();
    for(int i=0; i<DATA_CHANNEL_SIZE; i++)
    {
        /* Obtém angulos opostos na roda de cores */
        int hue = (int) 359*(i/((float)DATA_CHANNEL_SIZE));
        colorList.append(QColor::fromHsv(hue, 255, 255));
    }
    QVector<double> data1;
    QVector<double> data2;

    switch(index)
    {
        case COMBOBOX_CORRENTE_ELETRICA:
        {
            data1 = sensorData.data1;
            data2 = sensorData.data2;
        } break;
        case COMBOBOX_CONSUMO:
        {
            for(int i=0; i<sensorData.data1.size() - 1; i++)
            {
                double dt = (sensorData.timestamp[i+1] - sensorData.timestamp[i])/3600.0;
                data1.append(sensorData.data1[i]*voltage*dt/1000.0);
                data2.append(sensorData.data2[i]*voltage*dt/1000.0);
            }
        } break;
        case COMBOBOX_CONSUMO_ACUMULADO:
        {
            for(int i=0; i<sensorData.data1.size() - 1; i++)
            {
                double dt = (sensorData.timestamp[i+1] - sensorData.timestamp[i])/3600.0;
                data1.append(sensorData.data1[i]*voltage*dt/1000.0);
                data2.append(sensorData.data2[i]*voltage*dt/1000.0);
            }
            for(int i=1; i<sensorData.data1.size() - 1; i++)
            {
                data1[i] = data1[i] + data1[i-1];
                data2[i] = data2[i] + data2[i-1];
            }
        } break;
        case COMBOBOX_CUSTO:
        {
            for(int i=0; i<sensorData.data1.size() - 1; i++)
            {
                double dt = (sensorData.timestamp[i+1] - sensorData.timestamp[i])/3600.0;
                data1.append(sensorData.data1[i]*voltage*dt*custo*bandeira/1000.0);
                data2.append(sensorData.data2[i]*voltage*dt*custo*bandeira/1000.0);
            }
        } break;
        case COMBOBOX_CUSTO_ACUMULADO:
        {
            for(int i=0; i<sensorData.data1.size() - 1; i++)
            {
                double dt = (sensorData.timestamp[i+1] - sensorData.timestamp[i])/3600.0;
                data1.append(sensorData.data1[i]*voltage*dt*custo*bandeira/1000.0);
                data2.append(sensorData.data2[i]*voltage*dt*custo*bandeira/1000.0);
            }
            for(int i=1; i<sensorData.data1.size() - 1; i++)
            {
                data1[i] = data1[i] + data1[i-1];
                data2[i] = data2[i] + data2[i-1];
            }
        } break;
    };

    graphPlotAdd(0,1, sensorData.timestamp, data1);
    graphPlotAdd(1,2, sensorData.timestamp, data2);
    plotTitle = combobox_medida_labels[index];
    graphPlotShow(combobox_medida_unidades[index]);
}

/************************************************************************************
 *                                  DADOS E DIAS                                    *
 * *********************************************************************************/
/* Obter dados do coletor */
void MainWindow::on_SensorDataButton_clicked()
{
    /* dados */
    ui->SensorDataButton->setEnabled(false);

    /* calendar */
    QString dateInit = ui->DateCalendar->selectedDate().toString("yyyy-MM-dd");
    QString dateFinal = ui->DateCalendar->selectedDate().addDays(1).toString("yyyy-MM-dd");
    ui->DateCalendar->setEnabled(false);

    /* progressBar */
    ui->progressBar->setRange(0,0);
    ui->progressBar->reset();
    ui->progressBar->setEnabled(true);

    /* Inicializa requisição */
    QNetworkRequest request = QNetworkRequest(QUrl("http://api.thingspeak.com/channels/" + ui->lineEdit_channel->text() + "/feeds?api_key=" + ui->lineEdit_key->text() + "&start=" + dateInit + "&end=" + dateFinal));
    networkManager->get(request);

    ui->textBrowser->setText("<html><b>Dados</html></b>: Carregando dados. Esta operação poderá demorar alguns segundos.");
}

/* Botão para limpar dados armazenados */
void MainWindow::on_SensorDateClearButton_clicked()
{
    /* Data */
    measures.clear_sensorData();

    /* DateTable */
    ui->DateTableWidget->clearSelection();
    DateTableWidget_refresh();

    ui->textBrowser->setText("<html><b>Dados</html></b>: Dados armazenados excluidos.");
}

/* Mudança nos dias selecionados na tabela */
void MainWindow::on_DateTableWidget_itemSelectionChanged()
{
    /* Obtém a lista de dias selecionados */
    refresh_date_list(selectedDate);

    if(selectedDate.isEmpty())
    {
        ui->FileSaveButton->setEnabled(false);
    }
    else
    {
        ui->FileSaveButton->setEnabled(true);
    }

    /* Atualiza o gráfico */
    refresh_plot(ui->comboBox_medida->currentIndex());
}

/* Atualiza a tabela com os dias carregados */
void MainWindow::DateTableWidget_refresh()
{
    /* Limpa a tabela e popula novamente */
    ui->SensorDateClearButton->setEnabled(false);   /* Desativa botão de limpar dados */
    ui->DateTableWidget->blockSignals(true);        /* Desativa interrupções */
    ui->DateTableWidget->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->DateTableWidget->clearContents();
    ui->DateTableWidget->setRowCount(0);
    QList<QPair<QDate,quint32>> sensorDateList = measures.get_active_dateList();
    for(int i=0; i<sensorDateList.size(); i++)
    {
        QTableWidgetItem* tableItemDate = new QTableWidgetItem(sensorDateList[i].first.toString("dd/MM/yyyy (ddd)"),0);
        QTableWidgetItem* tableItemSize = new QTableWidgetItem(QString::number(sensorDateList[i].second),1);
        ui->DateTableWidget->insertRow(i);
        ui->DateTableWidget->setItem(i,0,tableItemDate);
        ui->DateTableWidget->setItem(i,1,tableItemSize);
        ui->SensorDateClearButton->setEnabled(true);    /* Ativa botão de limpar dados */
        if(selectedDate.contains(sensorDateList[i].first))
            ui->DateTableWidget->selectRow(i);  /* Seleciona novamente os dias */
    }
    ui->DateTableWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->DateTableWidget->blockSignals(false);        /* Re-ativa interrupções */

    /* Atualiza o gráfico */
    refresh_plot(ui->comboBox_medida->currentIndex());
}

/* Atualiza a lista de dias selecionados */
void MainWindow::refresh_date_list(QList<QDate> &dateList)
{
    dateList.clear();
    QList<QTableWidgetItem*> QTableWidgetItemList = ui->DateTableWidget->selectedItems();
    for(int i=0; i<QTableWidgetItemList.size(); i++)
    {
        QDate date = QDate::fromString(QTableWidgetItemList[i]->data(0).toString(),"dd/MM/yyyy (ddd)");
        if(date.isValid())
            dateList.append(date);
    }
}

/************************************************************************************
 *                              CARREGAR/SALVAR ARQUIVO                             *
 * *********************************************************************************/
/* Carregar arquivo no formato de protocolo */
void MainWindow::on_fileLoadButton_clicked()
{
    /* Comportamento da janela de escolha do arquivo */
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter("Javascript Object Notation (*.json)");

    /* Escolha do arquivo */
    if (dialog.exec())
    {
        QFile file(dialog.selectedFiles().first());

        if(!file.exists())
          return;

        if(!file.open(QIODevice::ReadOnly))
          return;

        /* Leitura de todos os dados */
        QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
        file.close();

        /* Feeds */
        QJsonArray feedsArray = doc.object().value("feeds").toArray();
        ui->textEdit_checkConnected->append("\r\nRecebido " + QString::number(feedsArray.size()) + " dados.");

        for(int i=0; i<feedsArray.size(); i++)
        {
            QJsonObject feeds = feedsArray.at(i).toObject();
            measures.set_sensorData(feeds);
        }

        /* Popula a DateTable com lista de dias carregados */
        DateTableWidget_refresh();

        ui->textBrowser->setText("<html><b>Carregar do Arquivo</html></b>: Dados carrregados com sucesso.");
    }
    else
        ui->textBrowser->setText("<html><b>Carregar do Arquivo</html></b>: Nenhum arquivo selecionado.");
}

/* Salva dados selecionados no arquivo */
void MainWindow::on_FileSaveButton_clicked()
{
    /* Formatos de arquivo disponíveis */
    QStringList filetype;
    filetype.append("Comma-Separated Values (*.csv)");
    filetype.append("Javascript Object Notation (*.json)");

    /* Comportamento da janela de escolha do arquivo */
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilters(filetype);

    /* Escolha do arquivo para salvar */
    if (dialog.exec())
    {
        QFile file(dialog.selectedFiles().first());

        if(file.exists())
          file.remove();

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
          return;

        /* Obtém a lista de dias selecionados */
        refresh_date_list(selectedDate);

        /* Verifica se a lista de dias selecionados não está vazia */
        if(!selectedDate.isEmpty())
        {
            Measures::sensor_data_t sensorData = measures.get_sensorData(selectedDate);

            /* Adiciona o tipo de separador no arquivo CSV */
            /* Adiciona headers */
            /* Escreve em formato .csv */
            if(dialog.selectedNameFilter() == "Comma-Separated Values (*.csv)")
            {
                file.write("sep=,\n");
                file.write("Data,Hora,Numero da Mensagem,Entrada 1 [A],Entrada 2 [A]\n");

                for(int i=0; i<sensorData.timestamp.size(); i++)
                {
                    QDateTime timestamp = QDateTime::fromSecsSinceEpoch(qint64(sensorData.timestamp[i]), Qt::UTC).toLocalTime();
                    file.write(timestamp.date().toString("dd/MM/yy").toLatin1() + "," + timestamp.time().toString("hh:mm:ss").toLatin1() + "," + QByteArray::number(sensorData.entryID[i]) + "," + QByteArray::number(sensorData.data1[i]) + "," + QByteArray::number(sensorData.data2[i]) + "\n");
                }
            }
            /* Escreve em formato .json */
            else
            {
                QJsonObject feeds;
                QJsonArray array;
                for(int i=0; i<sensorData.timestamp.size(); i++)
                {
                    QJsonObject json;
                    json.insert("created_at", QDateTime::fromSecsSinceEpoch(qint64(sensorData.timestamp[i]), Qt::UTC).toString(Qt::ISODate));
                    json.insert("entry_id", sensorData.entryID[i]);
                    json.insert("field1", QString::number(qint64(sensorData.data1[i]*1000)));
                    json.insert("field2", QString::number(qint64(sensorData.data2[i]*1000)));
                    array.append(QJsonValue(json));
                }
                feeds.insert("feeds", QJsonValue(array));
                QJsonDocument doc(feeds);
                file.write(doc.toJson());
            }
        }

        file.close();

        ui->textBrowser->setText("<html><b>Salvar no Arquivo</html></b>: Dados salvos no arquivo com sucesso.");
    }
    else
        ui->textBrowser->setText("<html><b>Salvar no Arquivo</html></b>: Nenhum arquivo selecionado.");
}

/************************************************************************************
*                              ARQUIVO CONFIGURAÇÕES                                *
* *********************************************************************************/
//* Salva configurações no arquivo */
void MainWindow::save_configuration(void)
{
    QFile file("config.json");
    if(file.exists())
        file.remove();
    
    QJsonObject json
    {
        { config_type_labels[CONFIG_CHANNEL], ui->lineEdit_channel->text() },
        { config_type_labels[CONFIG_KEY], ui->lineEdit_key->text() },
        { config_type_labels[CONFIG_VOLTAGE], voltage },
        { config_type_labels[CONFIG_COST], custo },
        { config_type_labels[CONFIG_FLAG], ui->comboBox_bandeira->currentIndex() },
    };
    QJsonDocument doc(json);
    
    if(!file.open(QIODevice::WriteOnly))
        return;
    file.write(doc.toJson());
    file.close();

    ui->textBrowser->setText("<html><b>Salvar Configurações</html></b>: Salvo com sucesso.");
    return;
}

/* Carrega configurações no arquivo */
void MainWindow::load_configuration(void)
{
    QFile file("config.json");
    if(!file.exists())
        return;

    if(!file.open(QIODevice::ReadOnly))
        return;
    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    file.close();

    QJsonObject json = doc.object();
    ui->lineEdit_channel->setText(json.value(config_type_labels[CONFIG_CHANNEL]).toString());
    ui->lineEdit_key->setText(json.value(config_type_labels[CONFIG_KEY]).toString());
    voltage = json.value(config_type_labels[CONFIG_VOLTAGE]).toInt();
    if(voltage == 220)
        ui->radioButton_220v->setChecked(true);
    else
        ui->radioButton_127v->setChecked(true);
    ui->doubleSpinBox_custo->setValue(json.value(config_type_labels[CONFIG_COST]).toDouble());
    ui->comboBox_bandeira->setCurrentIndex(json.value(config_type_labels[CONFIG_FLAG]).toInt());

    ui->textBrowser->setText("<html><b>Carregar Configurações</html></b>: Carregado com sucesso.");
    return;
}

/************************************************************************************
*                              SERVIDOR                                             *
* *********************************************************************************/
void MainWindow::networkFinished(QNetworkReply* reply)
{

    QByteArray response = reply->readAll();

    if(response == "-1")
    {
        ui->textBrowser->setText("<html><b>Conectar ao servidor</html></b>: Error de identificação. Verificar os campos inseridos.");
        ui->textEdit_checkConnected->clear();
    }
    else
    {
        ui->textBrowser->setText("<html><b>Conectar ao servidor</html></b>: Recebido com sucesso.");

        QJsonDocument json = QJsonDocument::fromJson(response);

        /* Channel */
        QJsonObject channel = json.object().value("channel").toObject();
        ui->textEdit_checkConnected->setText("Connectado com sucesso à: ");
        ui->textEdit_checkConnected->append(channel.value("name").toString() + "\r\n");
        ui->textEdit_checkConnected->append(channel.value("description").toString());

        /* Feeds */
        QJsonArray feedsArray = json.object().value("feeds").toArray();
        ui->textEdit_checkConnected->append("\r\nRecebido " + QString::number(feedsArray.size()) + " dados.");

        for(int i=0; i<feedsArray.size(); i++)
        {
            QJsonObject feeds = feedsArray.at(i).toObject();
            measures.set_sensorData(feeds);
        }

        /* Desativa Progressbar */
        ui->progressBar->setRange(0,100);
        ui->progressBar->setValue(100);
        ui->progressBar->setEnabled(false);

        /* Atualiza a lista de dias */
        DateTableWidget_refresh();

        ui->DateCalendar->setEnabled(true);
        ui->SensorDataButton->setEnabled(true);
    }

    ui->pushButton_connect->setEnabled(true);
    reply->deleteLater();
}

void MainWindow::on_pushButton_connect_clicked()
{
    /* Verifica se os campos estão preenchidos */
    if(ui->lineEdit_channel->text().isEmpty() || ui->lineEdit_key->text().isEmpty())
    {
        ui->textBrowser->setText("<html><b>Conectar ao servidor</html></b>: Preencha os dados do servidor.");
        return;
    }

    /* Não permite editar os campos */
    ui->lineEdit_key->setEnabled(false);
    ui->lineEdit_channel->setEnabled(false);
    ui->pushButton_connect->setEnabled(false);

    /* Inicializa requisição */
    QNetworkRequest request = QNetworkRequest(QUrl("http://api.thingspeak.com/channels/" + ui->lineEdit_channel->text() + "/feeds?api_key=" + ui->lineEdit_key->text() + "&results=0"));
    networkManager->get(request);

    ui->textBrowser->setText("<html><b>Conectar ao servidor</html></b>: Conectando.");
}

void MainWindow::on_lineEdit_key_textChanged(const QString &arg1)
{
    (void)arg1;
    if(ui->lineEdit_key->text().length() < 16 || ui->lineEdit_channel->text().isEmpty())
        ui->pushButton_connect->setEnabled(false);
    else
        ui->pushButton_connect->setEnabled(true);
}

void MainWindow::on_lineEdit_channel_textChanged(const QString &arg1)
{
    (void)arg1;
    if(ui->lineEdit_key->text().length() < 16 || ui->lineEdit_channel->text().isEmpty())
        ui->pushButton_connect->setEnabled(false);
    else
        ui->pushButton_connect->setEnabled(true);
}

void MainWindow::on_pushButton_edit_clicked()
{
    ui->lineEdit_key->setEnabled(true);
    ui->lineEdit_channel->setEnabled(true);
}

/************************************************************************************
 *                              MEDIDA ELÉTRICA                                     *
 * *********************************************************************************/
void MainWindow::on_comboBox_medida_currentIndexChanged(int index)
{
    /* Atualiza o gráfico */
    refresh_plot(index);
}

void MainWindow::on_radioButton_127v_clicked()
{
    /* Atualiza o gráfico */
    voltage = 127;
    refresh_plot(ui->comboBox_medida->currentIndex());
}

void MainWindow::on_radioButton_220v_clicked()
{
    /* Atualiza o gráfico */
    voltage = 220;
    refresh_plot(ui->comboBox_medida->currentIndex());
}

void MainWindow::on_comboBox_bandeira_currentIndexChanged(int index)
{
    /* Atualiza o gráfico */
    bandeira = combobox_bandeira_valor[index];
    refresh_plot(ui->comboBox_medida->currentIndex());
}

void MainWindow::on_doubleSpinBox_custo_valueChanged(double arg1)
{
    /* Atualiza o gráfico */
    custo = arg1;
    refresh_plot(ui->comboBox_medida->currentIndex());
}

/************************************************************************************
 *                              EXIT                             *
 * *********************************************************************************/
void MainWindow::closeEvent(QCloseEvent *event)
{
    (void)event;
    save_configuration();
}
